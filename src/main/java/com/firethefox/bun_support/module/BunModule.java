package com.firethefox.bun_support.module;

import com.firethefox.bun_support.BunIcons;
import com.intellij.ide.util.projectWizard.ModuleWizardStep;
import com.intellij.ide.util.projectWizard.WizardContext;
import com.intellij.openapi.module.ModuleType;
import com.intellij.openapi.module.ModuleTypeManager;
import com.intellij.openapi.roots.ui.configuration.ModulesProvider;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class BunModule extends ModuleType<BunModuleBuilder> {
    private static final String ID = "BUN_MODULE";

    public BunModule() {
        super(ID);
    }

    public static BunModule getInstance() {
        return (BunModule) ModuleTypeManager.getInstance().findByID(ID);
    }

    @NotNull
    @Override
    public BunModuleBuilder createModuleBuilder() {
        return new BunModuleBuilder();
    }

    @NotNull
    @Override
    public String getName() {
        return "Bun.Js Project";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Setup bun.js compatible project";
    }

    @NotNull
    @Override
    public Icon getNodeIcon(@Deprecated boolean b) {
        return BunIcons.icon;
    }

    @Override
    public ModuleWizardStep @NotNull [] createWizardSteps(@NotNull WizardContext wizardContext,
                                                          @NotNull BunModuleBuilder moduleBuilder,
                                                          @NotNull ModulesProvider modulesProvider) {
        return super.createWizardSteps(wizardContext, moduleBuilder, modulesProvider);
    }
}
