package com.firethefox.bun_support.module;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class BunModuleUI {
    private JPanel main;
    private JLabel version;

    public boolean valid = false;

    public BunModuleUI() {
        String home = System.getProperty("user.home");
        home += "/.bun/bin/bun";
        File f = new File(home);
        if (f.exists() && !f.isDirectory()) {
            ProcessBuilder processBuilder = new ProcessBuilder();
            processBuilder.command(home, "--version");

            Process process = null;
            try {
                process = processBuilder.start();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;

            while (true) {
                try {
                    if ((line = reader.readLine()) == null) break;
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                version.setText(line);
                valid = true;
            }

            try {
                int exitCode = process.waitFor();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        } else {
            version.setText("Is bun.js installed?");
        }
    }

    public JComponent getUI() {
        return main;
    }
}
