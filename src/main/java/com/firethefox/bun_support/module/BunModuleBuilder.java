package com.firethefox.bun_support.module;

import com.firethefox.bun_support.BunIcons;
import com.firethefox.bun_support.ResourcesLoader;
import com.intellij.ide.util.projectWizard.*;
import com.intellij.openapi.Disposable;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ModifiableRootModel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class BunModuleBuilder extends ModuleBuilder {
    boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }

    @Override
    public void setupRootModel(@NotNull ModifiableRootModel model) {
        Project project = model.getProject();
        String base_path = project.getBasePath();
        String bun = "cd ";
        bun += base_path;
        bun += " && ";
        bun += System.getProperty("user.home");
        bun += "/.bun/bin/bun";

        System.out.println(model.getContentRoots().length);

        deleteDirectory(new File(base_path + "/.idea"));

        try {

            FileWriter index = new FileWriter(base_path + "/index.ts");
            index.write(new String(ResourcesLoader.getFileContent("index.ts").readAllBytes(), StandardCharsets.UTF_8));
            index.close();

            FileWriter config = new FileWriter(base_path + "/tsconfig.json");
            config.write(new String(ResourcesLoader.getFileContent("tsconfig.json").readAllBytes(), StandardCharsets.UTF_8));
            config.close();

            FileWriter pkg = new FileWriter(base_path + "/package.json");
            String data = new String(ResourcesLoader.getFileContent("package.json").readAllBytes(), StandardCharsets.UTF_8);
            data = data.replace("{PROJ_NAME}", project.getName());
            pkg.write(data);
            pkg.close();

            FileWriter readme = new FileWriter(base_path + "/README.md");
            String data2 = new String(ResourcesLoader.getFileContent("README.md").readAllBytes(), StandardCharsets.UTF_8);
            data2 = data2.replace("{PROJ_NAME}", project.getName());
            readme.write(data2);
            readme.close();

            bun += " install";

            ProcessBuilder processBuilder = new ProcessBuilder();
            processBuilder.command("/usr/bin/bash", "-c", bun);

            Process process = null;
            process = processBuilder.start();

            try {
                int exitCode = process.waitFor();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public BunModule getModuleType() {
        return BunModule.getInstance();
    }

    @Nullable
    @Override
    public ModuleWizardStep getCustomOptionsStep(WizardContext context, Disposable parentDisposable) {
        return new BunModuleSteps();
    }
}
