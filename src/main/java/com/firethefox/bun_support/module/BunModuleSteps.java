package com.firethefox.bun_support.module;

import com.firethefox.bun_support.ResourcesLoader;
import com.intellij.ide.util.projectWizard.ModuleWizardStep;
import com.intellij.openapi.options.ConfigurationException;

import javax.swing.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class BunModuleSteps extends ModuleWizardStep {
    private final BunModuleUI ui = new BunModuleUI();
    @Override
    public JComponent getComponent() {
        return ui.getUI();
    }

    @Override
    public void updateDataModel() {
        //todo update model according to UI
    }

    @Override
    public boolean validate() throws ConfigurationException {
        return ui.valid;
    }
}
