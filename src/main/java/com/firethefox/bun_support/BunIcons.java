package com.firethefox.bun_support;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public interface BunIcons {
    Icon icon = IconLoader.getIcon("/logo.svg", BunIcons.class);
}
