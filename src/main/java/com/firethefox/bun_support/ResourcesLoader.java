package com.firethefox.bun_support;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class ResourcesLoader {
    public static InputStream getFileContent(String filename) {
        return ResourcesLoader.class.getClassLoader().getResourceAsStream(filename);
    }
}
