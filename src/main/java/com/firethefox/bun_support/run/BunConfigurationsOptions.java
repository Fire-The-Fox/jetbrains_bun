package com.firethefox.bun_support.run;

import com.intellij.execution.configurations.RunConfigurationOptions;
import com.intellij.openapi.components.StoredProperty;

public class BunConfigurationsOptions  extends RunConfigurationOptions {
    private final StoredProperty<String> params = string("").provideDelegate(this, "params");
    private final StoredProperty<String> args = string("").provideDelegate(this, "args");

    private final StoredProperty<String> action = string("dev").provideDelegate(this, "action");

    public String getAction() {
        return action.getValue(this);
    }

    public void setAction(String str) {
        action.setValue(this, str);
    }

    public String getParams() {
        return params.getValue(this);
    }

    public void setParams(String str) {
        params.setValue(this, str);
    }

    public String getArgs() {
        return args.getValue(this);
    }

    public void setArgs(String str) {
        args.setValue(this, str);
    }
}
