package com.firethefox.bun_support.run;

import com.firethefox.bun_support.BunIcons;
import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.configurations.ConfigurationType;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class BunConfigurationsType implements ConfigurationType {
    static final String ID = "BunConfigurations";

    @NotNull
    @Override
    public String getDisplayName() {
        return "Bun.Js Actions";
    }

    @Override
    public String getConfigurationTypeDescription() {
        return "Setup bun.js execution action";
    }

    @Override
    public Icon getIcon() {
        return BunIcons.icon;
    }

    @NotNull
    @Override
    public String getId() {
        return ID;
    }

    @Override
    public ConfigurationFactory[] getConfigurationFactories() {
        return new ConfigurationFactory[] { new BunConfigurationsFactory(this) };
    }
}
