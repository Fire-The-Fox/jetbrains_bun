package com.firethefox.bun_support.run;

import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.configurations.ConfigurationType;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.openapi.components.BaseState;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BunConfigurationsFactory extends ConfigurationFactory {
    protected BunConfigurationsFactory(ConfigurationType type) {
        super(type);
    }

    @Override
    public @NotNull String getId() {
        return BunConfigurationsType.ID;
    }

    @NotNull
    @Override
    public RunConfiguration createTemplateConfiguration(@NotNull Project project) {
        return new BunConfigurations(project, this, "Demo");
    }

    @Nullable
    @Override
    public Class<? extends BaseState> getOptionsClass() {
        return BunConfigurationsOptions.class;
    }
}
