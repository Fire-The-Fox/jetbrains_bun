package com.firethefox.bun_support.run;

import com.intellij.execution.ExecutionException;
import com.intellij.execution.Executor;
import com.intellij.execution.configurations.*;
import com.intellij.execution.process.OSProcessHandler;
import com.intellij.execution.process.ProcessHandler;
import com.intellij.execution.process.ProcessHandlerFactory;
import com.intellij.execution.process.ProcessTerminatedListener;
import com.intellij.execution.runners.ExecutionEnvironment;
import com.intellij.openapi.options.SettingsEditor;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BunConfigurations extends RunConfigurationBase<BunConfigurationsOptions> {
    public BunConfigurations(Project project, ConfigurationFactory factory, String name) {
        super(project, factory, name);
    }

    @NotNull
    @Override
    protected BunConfigurationsOptions getOptions() {
        return (BunConfigurationsOptions) super.getOptions();
    }

    public String getParams() {
        return getOptions().getParams();
    }

    public void setParams(String str) {
        getOptions().setParams(str);
    }

    public String getArgs() {
        return getOptions().getArgs();
    }

    public void setArgs(String str) {
        getOptions().setArgs(str);
    }

    public String getAction() {
        return getOptions().getAction();
    }

    public void setAction(String str) {
        getOptions().setAction(str);
    }

    @NotNull
    @Override
    public SettingsEditor<? extends RunConfiguration> getConfigurationEditor() {
        return new BunConfigurationsUI();
    }

    @Override
    public void checkConfiguration() {

    }

    @Nullable
    @Override
    public RunProfileState getState(@NotNull Executor executor, @NotNull ExecutionEnvironment executionEnvironment) {
        return new CommandLineState(executionEnvironment) {
            @NotNull
            @Override
            protected ProcessHandler startProcess() throws ExecutionException {
                String cmd_base = System.getProperty("user.home");
                cmd_base += "/.bun/bin/bun ";
                cmd_base += getAction();

                if (getParams() != null) {
                    cmd_base += " ";
                    cmd_base += getParams();
                }

                if (getArgs() != null) {
                    cmd_base += " ";
                    cmd_base += getArgs();
                }

                GeneralCommandLine commandLine = new GeneralCommandLine("/usr/bin/bash", "-c", cmd_base);
                commandLine.setWorkDirectory(executionEnvironment.getProject().getBasePath());
                OSProcessHandler processHandler = ProcessHandlerFactory.getInstance().createColoredProcessHandler(commandLine);
                ProcessTerminatedListener.attach(processHandler);
                return processHandler;
            }
        };
    }
}
