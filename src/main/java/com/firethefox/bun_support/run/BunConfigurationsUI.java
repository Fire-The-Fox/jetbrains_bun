package com.firethefox.bun_support.run;

import com.intellij.openapi.options.SettingsEditor;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class BunConfigurationsUI extends SettingsEditor<BunConfigurations> {
    private JPanel myPanel;
    private JComboBox comboBox1;
    private JTextField argInput;
    private JTextField paramInput;

    @Override
    protected void resetEditorFrom(@NotNull BunConfigurations conf) {
        argInput.setText(conf.getArgs());
        paramInput.setText(conf.getParams());
        comboBox1.setSelectedItem(conf.getAction());
    }

    @Override
    protected void applyEditorTo(@NotNull BunConfigurations conf) {
        conf.setArgs(argInput.getText());
        conf.setParams(paramInput.getText());
        conf.setAction((String) comboBox1.getSelectedItem());
    }

    @NotNull
    @Override
    protected JComponent createEditor() {
        return myPanel;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
